//
//  setOperation.hpp
//  Matzov C++ Modular Calculator
//
//  Created by Roman Fourman on 12/08/2017.
//  Copyright © 2017 Roman Fourman. All rights reserved.
//

#ifndef setOperation_h
#define setOperation_h

#include "calculatorOperation.h"

class setOperation : public calculatorOperation
{
public:
    bool supportsOperation(string op_string);
    int requiredParameters();
    double calcResult(double currVal, vector<string> parameters);

};

#endif /* setOperation_h */
