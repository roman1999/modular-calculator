//
//  stringParser.cpp
//  Matzov C++ Modular Calculator
//
//  Created by Roman Fourman on 12/08/2017.
//  Copyright © 2017 Roman Fourman. All rights reserved.
//

#include "stringParser.hpp"

string stringParser::operation(string command)
{
    return command.substr(0, command.find(' '));
}

vector<string> stringParser::parameters(string command)
{
    vector<string> parameters = vector<string>();
    string temp = string(command);
    temp += " ";
    
    int pos = 0;
    std::string token;
    pos = temp.find(' ');
    temp.erase(0, pos + 1);
    while ((pos = temp.find(' ')) != std::string::npos)
    {
        token = temp.substr(0, pos);
        parameters.push_back(token);
        temp.erase(0, pos + 1);
    }
    return parameters;
}

string stringParser::cleanSpaces(string command)
{
    string temp = string(command);
    int strBeginSpaces = temp.find_first_not_of(" ");
    if (strBeginSpaces != std::string::npos)
        temp.erase(0, strBeginSpaces);
    
    int strEndSpaces = temp.find_last_not_of(" ");
    if (strEndSpaces != string::npos)
        temp.erase(strEndSpaces + 1);
    
    int beginSpaceIndex = temp.find_first_of(' ');
    int endSpaceIndex = -1;
    int range = 0;
    int newStart = -1;
    while (beginSpaceIndex != std::string::npos)
    {
        endSpaceIndex = temp.find_first_not_of(' ', beginSpaceIndex);
        range = endSpaceIndex - beginSpaceIndex;
        
        temp.replace(beginSpaceIndex, range, " ");
        
        newStart = beginSpaceIndex + 1;
        beginSpaceIndex = temp.find_first_of(' ', newStart);
    }
    return temp;
}
