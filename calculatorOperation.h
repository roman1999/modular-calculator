//
//  calculatorOperation.h
//  Matzov C++ Modular Calculator
//
//  Created by Roman Fourman on 12/08/2017.
//  Copyright © 2017 Roman Fourman. All rights reserved.
//

#include <string>
#include <vector>

#ifndef calculatorOperation_h
#define calculatorOperation_h

using namespace std;

class calculatorOperation
{
public:
    bool supportsOperation(string op_string);
    int requiredParameters();
    double calcResult(double currVal, vector<string> parameters);
};

#endif /* calculatorOperation_h */
