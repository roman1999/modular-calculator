//
//  stringParser.hpp
//  Matzov C++ Modular Calculator
//
//  Created by Roman Fourman on 12/08/2017.
//  Copyright © 2017 Roman Fourman. All rights reserved.
//

#ifndef stringParser_hpp
#define stringParser_hpp

#include <string>
#include <vector>

#include <iostream>

using namespace std;

class stringParser
{
public:
    static string operation(string command);
    static vector<string> parameters(string command);
    static string cleanSpaces(string command);
};

#endif /* stringParser_hpp */
