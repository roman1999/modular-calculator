//
//  setOperation.cpp
//  Matzov C++ Modular Calculator
//
//  Created by Roman Fourman on 12/08/2017.
//  Copyright © 2017 Roman Fourman. All rights reserved.
//

#include "setOperation.h"

bool setOperation::supportsOperation(string op_string)
{
    return true;
}

int setOperation::requiredParameters()
{
    return 0;
}

double setOperation::calcResult(double currVal, vector<string> parameters)
{
    return 0;
}
